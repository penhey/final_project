package com.penhey.finalproject.Screen.ApiService;


import com.penhey.finalproject.Screen.Login.LoginData;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import com.penhey.finalproject.Screen.Menu.MenuItemResponse;
import com.penhey.finalproject.Screen.Menu.MenuItemsResponse;
import com.penhey.finalproject.Screen.SignUp.SignUpData;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Multipart;
import retrofit2.http.Part;
import retrofit2.http.DELETE;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RetrofitInterface { // Interface for API calls using Retrofit

    @POST("/api/login")
    Call<LoginData> loginUser(@Body LoginData user);

    @POST("/api/signup")
    Call<SignUpData> signUpUser(@Body SignUpData signUp);

    @Multipart
    @POST("/api/menu-create")
    Call<MenuItemData> AddMenuItem(
            @Part("ItemName") RequestBody ItemName,
            @Part("Description") RequestBody Description,
            @Part("Price") RequestBody Price,
            @Part MultipartBody.Part imageData
    );

    // Get all menu items API route
    @GET("api/menu-getall")
    Call<MenuItemsResponse> getAllMenuItems();

    // Get single menu item by ID API route
    @GET("api/menu-getOne/{MenuID}")
    Call<MenuItemResponse> getMenuItemById(@Path("MenuID") int menuID);
    // Menu edit API route
   
    @PUT("api/menu-update/{MenuID}")
    @Multipart
    Call<MenuItemResponse> editMenuItem(
            @Path("MenuID") int menuID,
            @Part("ItemName") RequestBody ItemName,
            @Part("Description") RequestBody Description,
            @Part("Price") RequestBody Price,
            @Part MultipartBody.Part imageData);
    // Menu delete API route
    @DELETE("api/menuItem-remove/{MenuID}")
    Call<MenuItemResponse> deleteMenuItem(@Path("MenuID") int menuID);

    Throwable message();
}

