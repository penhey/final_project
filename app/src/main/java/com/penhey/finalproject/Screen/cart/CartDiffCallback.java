package com.penhey.finalproject.Screen.cart;

import androidx.recyclerview.widget.DiffUtil;

import com.penhey.finalproject.Screen.Menu.MenuItemData;

import java.util.List;

public class CartDiffCallback extends DiffUtil.Callback {
    private final List<MenuItemData> oldList;
    private final List<MenuItemData> newList;

    public CartDiffCallback(List<MenuItemData> oldList, List<MenuItemData> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        // Compare the unique IDs (menuID) of the old item and the new item.
        return oldList.get(oldItemPosition).getMenuID() == newList.get(newItemPosition).getMenuID();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        // Check whether the contents of the items are the same or not.
        MenuItemData oldItem = oldList.get(oldItemPosition);
        MenuItemData newItem = newList.get(newItemPosition);
        return oldItem.equals(newItem); // Ensure MenuItemData implements a proper equals method
    }
}

