package com.penhey.finalproject.Screen.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;


import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.ApiService.RetrofitClient;
import com.penhey.finalproject.Screen.ApiService.RetrofitInterface;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import com.penhey.finalproject.Screen.Menu.MenuItemResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;



public class MenuDetailFragment extends Fragment {
    private static final String ARG_MENU_ID = "menuID";
    private int menuId;
    private TextView textViewDetail, DetailViewDescription, textViewPrice;
    private ImageView imageMenuDetailView;
    private Button btnEdit, btnDelete, btnPayNow;
    private ImageButton btnBack;

    // Assuming MenuItemResponse correctly models your API response and MenuItemDetail is part of that response or the same
    public static MenuDetailFragment newInstance(int menuId) {
        MenuDetailFragment fragment = new MenuDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MENU_ID, menuId);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menuId = getArguments().getInt(ARG_MENU_ID);
        }
        Log.d("MenuDetailFragment", "Received menuID: " + menuId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menuitem_detail, container, false);
        initializeViews(view);
        fetchMenuItemDetails();
        adjustMenuItemsForRole(view);
        return view;
    }
    private void adjustMenuItemsForRole(View view) {
        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("UserData", Context.MODE_PRIVATE);
        String role = sharedPreferences.getString("userRole", "client"); // Default to client if no role found
        Log.d("MainActivity", "Role: " + role);
        if (!role.equals("admin")) {
            btnEdit.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
        }if(role.equals("admin")){
            btnPayNow.setVisibility(View.GONE);
        }

    }

    private void initializeViews(View view) {
        textViewDetail = view.findViewById(R.id.textViewDetail);
        DetailViewDescription = view.findViewById(R.id.DetailViewDescription);
        textViewPrice = view.findViewById(R.id.textViewPrice);
        imageMenuDetailView = view.findViewById(R.id.imageMenuDetailView);
        btnEdit = view.findViewById(R.id.EditMenuButton);
        btnDelete = view.findViewById(R.id.DeleteMenuButton);
        btnBack = view.findViewById(R.id.imageButtonBack);
        btnPayNow = view.findViewById(R.id.btnBuyNow);

        btnBack.setOnClickListener(v -> getParentFragmentManager().popBackStack());
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteConfirmationDialog();
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create an instance of the EditMenuItemFragment with arguments
                EditMenuItemFragment editFragment = EditMenuItemFragment.newInstance(menuId);

                requireActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, editFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    private void fetchMenuItemDetails() {
        RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        Call<MenuItemResponse> call = retrofitInterface.getMenuItemById(menuId);
        call.enqueue(new Callback<MenuItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<MenuItemResponse> call, @NonNull Response<MenuItemResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MenuItemResponse menuItemResponse = response.body();
                    Log.d("API Response", "Success: " + response.body());
                    if (menuItemResponse.isSuccess() && menuItemResponse.getMenuItem() != null) {
                        // Now you have your MenuItemDetail object
                        MenuItemData detail = menuItemResponse.getMenuItem();
                        updateUI(detail);
                    } else {
                        Toast.makeText(getActivity(), "Menu item details not found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Failed to load menu item details", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(@NonNull Call<MenuItemResponse> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUI(MenuItemData detail) {
        textViewDetail.setText(detail.getItemName());
        DetailViewDescription.setText(detail.getDescription());
        textViewPrice.setText(String.format("$%s", detail.getPrice()));
        Picasso.get().load(detail.getImageURL()).into(imageMenuDetailView);
        Log.d("MenuDetailFragment", "Item Name: " + detail.getItemName());
    }


    private void deleteMenuItem() {
        // Assuming RetrofitInterface contains a method for deleting menu items
        RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        Call<MenuItemResponse> call = retrofitInterface.deleteMenuItem(menuId); // Adjust according to your API method
        call.enqueue(new Callback<MenuItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<MenuItemResponse> call, @NonNull Response<MenuItemResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Menu item deleted successfully", Toast.LENGTH_SHORT).show();
                    getParentFragmentManager().popBackStack(); // Navigate back after deletion
                } else {
                    Toast.makeText(getActivity(), "Failed to delete menu item", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MenuItemResponse> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDeleteConfirmationDialog() {
        new AlertDialog.Builder(requireActivity())
                .setMessage("Are you sure you want to delete this menu item?")
                .setPositiveButton("Delete", (dialog, id) -> deleteMenuItem())
                .setNegativeButton("Cancel", (dialog, id) -> dialog.dismiss())
                .create().show();
    }
}
