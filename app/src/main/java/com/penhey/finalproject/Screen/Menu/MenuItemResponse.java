package com.penhey.finalproject.Screen.Menu;

import com.google.gson.annotations.SerializedName;

public class MenuItemResponse {

    @SerializedName("success")
    private boolean success;
    @SerializedName("menuItem")
    private MenuItemData menuItem; // Use the MenuItemData class for the "menuItem" object

    // Getter and Setter
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public MenuItemData getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItemData menuItem) {
        this.menuItem = menuItem;
    }


}


