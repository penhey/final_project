package com.penhey.finalproject.Screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.SharedPreferences;
import android.util.Log;
import android.view.MenuItem;

import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.Fragment.FavoriteFragment;
import com.penhey.finalproject.Screen.Fragment.HomeFragment;
import com.penhey.finalproject.Screen.Fragment.CartFragment;
import com.penhey.finalproject.Screen.Fragment.AddMenuFragment;
import com.penhey.finalproject.Screen.Fragment.SettingFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main  );
        // Initialize the BottomNavigationView
        BottomNavigationView bottomNav = findViewById(R.id.bottomNavigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        // Dynamically adjust BottomNavigationView items based on the user role
        adjustMenuItemsForRole(bottomNav);

        // Load HomeFragment initially
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new HomeFragment())
                    .commit();
        }
    }
    private void adjustMenuItemsForRole(BottomNavigationView bottomNav) {
        SharedPreferences sharedPreferences = getSharedPreferences("UserData", MODE_PRIVATE);
        String role = sharedPreferences.getString("userRole", "client"); // Default to client if no role found
        Log.d("MainActivity", "Role: " + role);

        // Hide the "Add" button for clients
        MenuItem addItem = bottomNav.getMenu().findItem(R.id.btnHomeAdd);
        boolean isAdmin = "admin".equals(role);
        Log.d("MainActivity", "Is Admin: " + isAdmin); // Debugging
        addItem.setVisible("admin".equals(role)); // Show for admin, hide for others
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {

                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    int itemId = item.getItemId();
                    Fragment selectedFragment = null;
                    // Switch between different fragments based on the item clicked
                    if (itemId == R.id.btnHomeMenu) {
                        selectedFragment = new HomeFragment();
                    } else if (itemId == R.id.btnHomeCart) {
                        selectedFragment = new CartFragment();
                    } else if (itemId == R.id.btnHomeAdd) {
                        selectedFragment = new AddMenuFragment();
                    } else if (itemId == R.id.btnHomeSetting) {
                        selectedFragment = new SettingFragment();
                    } else if(itemId == R.id.btnFavorite){
                        selectedFragment = new FavoriteFragment();
                    }
                    if (selectedFragment != null) {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, selectedFragment)
                                .commit();
                    }

                    return true;
                }

            };

}



