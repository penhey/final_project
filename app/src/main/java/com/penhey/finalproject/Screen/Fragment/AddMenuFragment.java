package com.penhey.finalproject.Screen.Fragment;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.ApiService.RetrofitClient;
import com.penhey.finalproject.Screen.ApiService.RetrofitInterface;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import com.penhey.finalproject.Screen.helper.FileUtils;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddMenuFragment extends Fragment {
    private EditText editTextItemName;
    private EditText editTextDescription;
    private EditText editTextPrice;
    private ImageView SelectImage;
    private Button buttonAdd;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri selectedImageUri;


    public AddMenuFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_addmenuitem, container, false);
        SelectImage = view.findViewById(R.id.ViewSelectedImage);
        editTextItemName = view.findViewById(R.id.editTxtTile);
        editTextDescription = view.findViewById(R.id.editDescriptionView);
        editTextPrice = view.findViewById(R.id.editPriceView);
        buttonAdd = view.findViewById(R.id.MenuSaveBtn);
        SelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadMenuItem();

            }
        });
        return view;

    }
    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
       /* intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        */
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            selectedImageUri = data.getData();
            Picasso.get().load(selectedImageUri).into(SelectImage);
        }
    }
    /*private String getPathFromUri(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(column_index);
        }
        if (cursor != null) {
            cursor.close();
        }
        return null;
    }

     */

    private void uploadMenuItem() {
        String itemName = editTextItemName.getText().toString();
        String description = editTextDescription.getText().toString();
        String price = editTextPrice.getText().toString();

        if (selectedImageUri == null) {
            Toast.makeText(getContext(), "Please select an image", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            // Get ContentResolver and MIME type
            ContentResolver contentResolver = getContext().getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String type = contentResolver.getType(selectedImageUri);
            if (type == null) {
                type = "image/*"; // Fallback type if unable to determine
            }

            // Open InputStream from URI
            InputStream inputStream = contentResolver.openInputStream(selectedImageUri);
            if (inputStream == null) {
                Toast.makeText(getContext(), "Failed to open image", Toast.LENGTH_SHORT).show();
                return;
            }

            // Create RequestBody from InputStream
            RequestBody requestFile = RequestBody.create(okhttp3.MediaType.parse(type), toByteArray(inputStream));
            MultipartBody.Part imageBody = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);

            // Other parts as before
            RequestBody itemNameBody = RequestBody.create(MediaType.parse("text/plain"), itemName);
            RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
            RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);

            // Retrofit call as before
            RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
            Call<MenuItemData> call = retrofitInterface.AddMenuItem(itemNameBody, descriptionBody, priceBody, imageBody);
            call.enqueue(new Callback<MenuItemData>() {
                @Override
                public void onResponse(@NonNull Call<MenuItemData> call, @NonNull Response<MenuItemData> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(getContext(), "Menu item added successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Failed to add menu item", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuItemData> call, @NonNull Throwable t) {
                    Toast.makeText(getContext(), "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (IOException e) {
            Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    // Helper method to convert InputStream to byte[]
    private byte[] toByteArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

}



