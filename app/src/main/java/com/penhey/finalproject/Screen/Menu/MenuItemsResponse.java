package com.penhey.finalproject.Screen.Menu;

import java.util.List;

public class MenuItemsResponse {
    private boolean success;
    private List<MenuItemData> menuItems;

    // Getters and setters
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<MenuItemData> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemData> menuItems) {
        this.menuItems = menuItems;
    }
}

