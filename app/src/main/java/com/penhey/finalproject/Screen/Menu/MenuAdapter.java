package com.penhey.finalproject.Screen.Menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.helper.MenuDiffCallback;
import com.squareup.picasso.Picasso;
import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    private List<MenuItemData> menuItems;
    private final LayoutInflater inflater;
    private final OnMenuItemClickListener itemClickListener;
    private OnMenuItemAddCartListener itemAddCartListener;

    public MenuAdapter(Context context, List<MenuItemData> menuItems, OnMenuItemClickListener itemClickListener, OnMenuItemAddCartListener itemAddCartListener) {
        this.inflater = LayoutInflater.from(context);
        this.menuItems = menuItems;
        this.itemClickListener = itemClickListener;
        this.itemAddCartListener = itemAddCartListener;
    }
    // Method to update the dataset within the adapter
    public void updateData(List<MenuItemData> newMenuItems) {
        // Create a new MenuDiffCallback with the old list and the new list
        MenuDiffCallback diffCallback = new MenuDiffCallback(this.menuItems, newMenuItems);
        // Calculate the diff
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        // Clear the old data and add all the new items
        this.menuItems.clear(); // Clear the existing items
        this.menuItems.addAll(newMenuItems); // Add all the new items
        // Dispatch updates to the adapter. This will trigger UI updates in an efficient manner.
        diffResult.dispatchUpdatesTo(this);
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_menu, parent, false); // Corrected to use item_menu.xml
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        MenuItemData menuItem = menuItems.get(position);
        holder.bind(menuItem, itemAddCartListener);
        holder.imgItem.setOnClickListener(v -> itemClickListener.onItemClick(menuItem.getMenuID()));
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    public static class MenuViewHolder extends RecyclerView.ViewHolder {
        TextView txtItemName, txtPrice;
        ImageView imgItem;
        ImageButton btnAddCart ;

        public MenuViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemName = itemView.findViewById(R.id.nameView);
            txtPrice = itemView.findViewById(R.id.priceView);
            imgItem = itemView.findViewById(R.id.imageMenuView);
            btnAddCart = itemView.findViewById(R.id.btnAddToCart);
        }

        void bind(MenuItemData menuItem, OnMenuItemAddCartListener itemAddCartListener) {
            txtItemName.setText(menuItem.getItemName());
            txtPrice.setText(String.format("$%s", menuItem.getPrice()));
            Picasso.get().load(menuItem.getImageURL()).into(imgItem);

            btnAddCart.setOnClickListener(v -> {
                if (itemAddCartListener != null) {
                    itemAddCartListener.onItemAddCart(menuItem);
                }
            });
        }

    }

    public interface OnMenuItemClickListener {
        void onItemClick(int menuID);
    }
    public interface  OnMenuItemAddCartListener{
        void onItemAddCart(MenuItemData menuID);
    }

}



