package com.penhey.finalproject.Screen.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import com.penhey.finalproject.Screen.cart.CartAdapter;
import com.penhey.finalproject.Screen.cart.SharedViewModel;
import java.util.ArrayList;


public class CartFragment extends Fragment {
    private SharedViewModel sharedViewModel;
    private CartAdapter cartAdapter;
    private RecyclerView recyclerView;


    public CartFragment() {
        // Required empty public constructor
    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        recyclerView = view.findViewById(R.id.cartListView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        CartAdapter.OnCartItemInteractionListener interactionListener = new CartAdapter.OnCartItemInteractionListener() {
            @Override
            public void onItemQuantityChanged(MenuItemData menuItem) {
                sharedViewModel.adjustItemQuantity(menuItem, menuItem.getQuantity());
            }

            @Override
            public void onItemRemoved(MenuItemData menuItem) {
                sharedViewModel.removeItemFromCart(menuItem);
            }
        };
        cartAdapter = new CartAdapter(getContext(), new ArrayList<>(), interactionListener);
        recyclerView.setAdapter(cartAdapter);
        sharedViewModel.getCartItems().observe(getViewLifecycleOwner(), cartItems -> {
            cartAdapter.updateData(cartItems);
        });

        return view;
    }
}