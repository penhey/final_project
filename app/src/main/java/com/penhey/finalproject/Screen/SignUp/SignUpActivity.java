package com.penhey.finalproject.Screen.SignUp;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.gson.Gson;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.ApiService.RetrofitClient;
import com.penhey.finalproject.Screen.ApiService.RetrofitInterface;
import com.penhey.finalproject.Screen.Login.LoginActivity;
import com.penhey.finalproject.Screen.Login.LoginData;
import com.penhey.finalproject.Screen.MainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends AppCompatActivity {
    private EditText usernameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private Button signUpButton;
    private TextView textMessageLogin, logIn;
    private RetrofitInterface retrofitInterface ;
    boolean isAllFieldsChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_screen);
        usernameEditText = findViewById(R.id.editTextSignUpUsername);
        emailEditText = findViewById(R.id.editTextEmail);
        passwordEditText = findViewById(R.id.editTextPassword);
        confirmPasswordEditText = findViewById(R.id.editTextcfPassword);
        signUpButton = findViewById(R.id.btnSignUp);
        textMessageLogin = findViewById(R.id.textMessageLogin);
        logIn = findViewById(R.id.logIn);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAllFieldsChecked = CheckAllFields();
                if(isAllFieldsChecked){
                    handleSignUpUser();
                }

            }
        });
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
               startActivity(intent);
               finish();
            }
        });


    }
    private void handleSignUpUser(){

        String username = usernameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();

        // Create a SignUp object with signup details
        SignUpData signUp = new SignUpData(username, email, password, confirmPassword, "client");

        // RetrofitInterface instance
        RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        // API call for user signup
        Call<SignUpData> call = retrofitInterface.signUpUser(signUp);
        call.enqueue(new Callback<SignUpData>() {
            @Override
            public void onResponse(@NonNull Call<SignUpData> call, @NonNull Response<SignUpData> response) {
                if (response.isSuccessful()) {
                    SignUpData SignedUpUser = response.body();
                    storeUserData(SignedUpUser);
                    signUpButton.setEnabled(true);
                    // Navigate to Login Screen
                    Toast.makeText(SignUpActivity.this, "SignUp successful.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent( SignUpActivity.this, MainActivity.class);
                    //handle clear activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    // Handle signup failure
                    Toast.makeText(SignUpActivity.this, "Signup failed", Toast.LENGTH_SHORT).show();
                    Log.d("SignUpActivity", "Request Body: " + new Gson().toJson(signUp));
                }
            }
            public void onFailure(@NonNull Call<SignUpData> call, @NonNull Throwable t) {
                // Handle network errors
                Toast.makeText(SignUpActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                Log.e("SignUpActivity", "Error: " + t.getMessage(), t);
                if (call.isCanceled()) {
                    Log.e("SignUpActivity", "Request was canceled");
                }

                // Print the response body for further analysis
                Log.d("SignUpActivity", "Response Body: " + t.getMessage());

            }
        });

    }
    private void storeUserData(SignUpData user) {
        SharedPreferences sharedPreferences = getSharedPreferences("UserData", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username", user.getUsername());
        editor.putString("email", user.getEmail());
        editor.putString("userRole", user.getUserRole());
        editor.apply();
    }
    private boolean CheckAllFields() {
        String password = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();
        if (usernameEditText.length() == 0) {
            usernameEditText.setError("Username field is required");
            return false;
        }

        if (emailEditText.length() == 0) {
            emailEditText.setError("Email field is required");
            return false;
        }
        if (password.length() == 0) {
            passwordEditText.setError("Password is required");
            return false;
        }
        if (!confirmPassword.equals(password)) {
            confirmPasswordEditText.setError("Password is not match!");
            return false;
        }
        return true;
    }

}




