package com.penhey.finalproject.Screen.ApiService;

public class ApiResponse<T> {
    private Status status;
    private String message;
    private T data;

    public ApiResponse(Status status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

    public enum Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}
