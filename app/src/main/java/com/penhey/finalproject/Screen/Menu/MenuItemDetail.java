package com.penhey.finalproject.Screen.Menu;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MenuItemDetail implements Parcelable {

    // Example fields
    @SerializedName("MenuID")
    private int menuID;
    @SerializedName("ItemName")
    private String itemName;
    @SerializedName("Description")
    private String description;
    @SerializedName("Price")
    private String price;
    @SerializedName("ImageURL")
    private String imageURL;

    // Constructor, getters, and setters

    protected MenuItemDetail(Parcel in) {
        menuID = in.readInt();
        itemName = in.readString();
        description = in.readString();
        price = in.readString();
        imageURL = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(menuID);
        dest.writeString(itemName);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeString(imageURL);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MenuItemDetail> CREATOR = new Creator<MenuItemDetail>() {
        @Override
        public MenuItemDetail createFromParcel(Parcel in) {
            return new MenuItemDetail(in);
        }

        @Override
        public MenuItemDetail[] newArray(int size) {
            return new MenuItemDetail[size];
        }
    };


    public int getMenuID() {
        return menuID;
    }

    public void setMenuID(int menuID) {
        this.menuID = menuID;
    }
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

}
