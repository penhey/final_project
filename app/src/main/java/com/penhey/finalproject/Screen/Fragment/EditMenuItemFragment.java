package com.penhey.finalproject.Screen.Fragment;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.ApiService.ApiResponse;
import com.penhey.finalproject.Screen.ApiService.RetrofitClient;
import com.penhey.finalproject.Screen.ApiService.RetrofitInterface;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import com.penhey.finalproject.Screen.Menu.MenuItemResponse;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditMenuItemFragment extends Fragment {

    private static final int PICK_IMAGE_REQUEST = 1;
    private static final String ARG_MENU_ID =  "menuID";
    private Uri selectedImageUri;
    private ImageView imageViewSelectedItem;
    private EditText editTextItemName, editTextDescription, editTextPrice;
    private Button buttonUpdate, buttonBack;
    private int menuId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_menu_item, container, false);

        // Initialize your views
        imageViewSelectedItem = view.findViewById(R.id.imageViewSelectedItem);
        editTextItemName = view.findViewById(R.id.editTextItemName);
        editTextDescription = view.findViewById(R.id.editTextDescription);
        editTextPrice = view.findViewById(R.id.editTextPrice);
        buttonUpdate = view.findViewById(R.id.buttonUpdate);
        buttonBack = view.findViewById(R.id.MenBackBtn);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getParentFragmentManager().popBackStack();
            }
        });

        // Load existing data into views
        loadMenuItemDetails(menuId);
        imageViewSelectedItem.setOnClickListener(v -> selectImage());
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateMenuItem(menuId);
            }
        });

        return view;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menuId = getArguments().getInt(ARG_MENU_ID);
            Log.e("menID: ", String.valueOf(menuId));
        }
    }

    private void updateUI(MenuItemData detail) {
        editTextItemName.setText(detail.getItemName());
        editTextDescription.setText(detail.getDescription());
        editTextPrice.setText(String.format("%s", detail.getPrice()));
        Picasso.get().load(detail.getImageURL()).into(imageViewSelectedItem);
        Log.e("MenuDetailFragment", "Item Name: " + detail.getItemName());
    }
    public static EditMenuItemFragment newInstance(int menuId) {
        EditMenuItemFragment fragment = new EditMenuItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MENU_ID, menuId);
        fragment.setArguments(args);
        return fragment;
    }
    private void loadMenuItemDetails(int menuId) {
        RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        Call<MenuItemResponse> call = retrofitInterface.getMenuItemById(menuId);
        call.enqueue(new Callback<MenuItemResponse>() {
            @Override
            public void onResponse(@NonNull Call<MenuItemResponse> call, @NonNull Response<MenuItemResponse> response) {

                if (response.isSuccessful() && response.body() != null) {
                    MenuItemResponse menuItemResponse = response.body();
                    Log.d("API Response", "Success: " + response.body());
                    if (menuItemResponse.isSuccess() && menuItemResponse.getMenuItem() != null) {
                        MenuItemData detail = menuItemResponse.getMenuItem();
                        updateUI(detail);
                    } else {
                        Toast.makeText(getActivity(), "Menu item details not found", Toast.LENGTH_SHORT).show();
                    }
                } else if (!response.isSuccessful()) {
                    Log.e("LoadMenuDetails", "Failed to load details, response code: " + response.code());
                    Toast.makeText(getActivity(), "Failed to load menu item details", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "Failed to load menu item details", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(@NonNull Call<MenuItemResponse> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("LoadMenuDetails", "Error loading menu details: " + t.getMessage(), t);
            }
        });
    }

    private void selectImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            selectedImageUri = data.getData();
            Picasso.get().load(selectedImageUri).into(imageViewSelectedItem);
        }
    }

    private void updateMenuItem(int menuId) {
        String updatedName = editTextItemName.getText().toString();
        String updatedDescription = editTextDescription.getText().toString();
        String updatedPrice = editTextPrice.getText().toString();

        MenuItemData updateMenuItem = new MenuItemData();
        updateMenuItem.setItemName(updatedName);
        updateMenuItem.setDescription(updatedDescription);
        updateMenuItem.setPrice(updatedPrice);

        try {
            ContentResolver contentResolver = getContext().getContentResolver();
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String type = contentResolver.getType(selectedImageUri);
            if (type == null) {
                type = "image/*"; // Fallback type if unable to determine
            }
            // Open InputStream from URI
            InputStream inputStream = contentResolver.openInputStream(selectedImageUri);
            if (inputStream == null) {
                Toast.makeText(getContext(), "Failed to open image", Toast.LENGTH_SHORT).show();
                return;
            }

            RequestBody requestFile = RequestBody.create(okhttp3.MediaType.parse(type), toByteArray(inputStream));
            MultipartBody.Part imageBody = MultipartBody.Part.createFormData("image", "image.jpg", requestFile);

            RequestBody itemNameBody = RequestBody.create(MediaType.parse("text/plain"), updatedName);
            RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), updatedDescription);
            RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), updatedPrice);


            RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
            Call<MenuItemResponse> call = retrofitInterface.editMenuItem(menuId,itemNameBody,descriptionBody, priceBody, imageBody);
            Log.e("MenuID", call.toString());
            Log.d("MenuID", call.toString());
            call.enqueue(new Callback<MenuItemResponse>() {
                @Override
                public void onResponse(@NonNull Call<MenuItemResponse> call, @NonNull Response<MenuItemResponse> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        Toast.makeText(getContext(), "Menu item updated successfully", Toast.LENGTH_SHORT).show();
                        assert getFragmentManager() != null;
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, new HomeFragment())
                                .addToBackStack(null)
                                .commit();
                    }else if(!response.isSuccessful()) {
                        try {
                            assert response.errorBody() != null;
                            ApiResponse errorResponse;
                            errorResponse = new Gson().fromJson(response.errorBody().string(), ApiResponse.class);
                            Toast.makeText(getContext(), errorResponse.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(), "An error occurred.", Toast.LENGTH_LONG).show();
                        }
                        Log.e("Update menuItem", "Failed to update details, response code: " + response.code());
                    }
                    else {
                        Toast.makeText(getContext(), "Failed to update menu item", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MenuItemResponse> call, @NonNull Throwable t) {
                    Log.e("LoadMenuDetails", "Error loading menu details: " + t.getMessage(), t);
                    Toast.makeText(getContext(), "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }catch (IOException e){
            Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
    private byte[] toByteArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

}
