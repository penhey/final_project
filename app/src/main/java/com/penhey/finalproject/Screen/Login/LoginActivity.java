package com.penhey.finalproject.Screen.Login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.ApiService.RetrofitClient;
import com.penhey.finalproject.Screen.ApiService.RetrofitInterface;
import com.penhey.finalproject.Screen.MainActivity;
import com.penhey.finalproject.Screen.SignUp.SignUpActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity { // Login activity

    private EditText usernameEditText;
    private EditText passwordEditText;
    private RetrofitInterface retrofitInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_screen);

        usernameEditText = findViewById(R.id.editTextUsername);
        passwordEditText = findViewById(R.id.editTextPass);
        Button loginButton = findViewById(R.id.btnSignIn);
        TextView navigateSignUp = findViewById(R.id.signUp);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleLoginUser();

            }
        });
        navigateSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
            }
        });
    }
    private void handleLoginUser() {
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        // Create a LoginData object with login credentials
        LoginData user = new LoginData(null, username, password, null);
        if(username.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Please fill in username.", Toast.LENGTH_SHORT).show();
        }else if(password.isEmpty()){
            Toast.makeText(LoginActivity.this, "Please fill in password.", Toast.LENGTH_SHORT).show();
        }else{
            // Retrofit instance
            retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
            // API call for user login
            Call<LoginData> call = retrofitInterface.loginUser(user);
            call.enqueue(new Callback<LoginData>() {
                @Override
                public void onResponse(@NonNull Call<LoginData> call, @NonNull Response<LoginData> response) {
                    if (response.isSuccessful()){
                        LoginData loggedInUser = response.body();
                        assert loggedInUser != null;
                        storeUserData(loggedInUser);
                        handleRoleBaseNavigation(loggedInUser.getUserRole());
                    }else{
                        // handle login failure
                        Toast.makeText(LoginActivity.this, "Login failed!", Toast.LENGTH_SHORT).show();
                        Log.d("LoginActivity", "Request Body: " + new Gson().toJson(user));
                    }
                }
                @Override
                public void onFailure(@NonNull Call<LoginData> call, @NonNull Throwable t) {
                    //handle network errors
                    Toast.makeText(LoginActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }

    }
    private void storeUserData(LoginData user) {
        SharedPreferences sharedPreferences = getSharedPreferences("UserData", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("user_id", user.getUserId());
        editor.putString("username", user.getUsername());
        editor.putString("full_name", user.getFullName());
        editor.putString("email", user.getEmail());
        editor.putString("bio", user.getBio());
        editor.putString("avatar_url", user.getAvatar_url());
        editor.putString("userRole", user.getUserRole());
        editor.apply();
    }
    private void handleRoleBaseNavigation(String role) {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        if ("admin".equals(role) || "client".equals(role)) {
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Unknown role, access denied.", Toast.LENGTH_LONG).show();
        }
    }

}



