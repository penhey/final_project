package com.penhey.finalproject.Screen.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import com.penhey.finalproject.R;


public class SettingFragment extends Fragment {
    private TableRow BtnProfile,BtnAboutUs,BtnDeleteProfile;
    ImageView imageProfile;


    public SettingFragment(){

    }

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        BtnProfile = view.findViewById(R.id.btnProfile);
        BtnAboutUs = view.findViewById(R.id.btnAboutUs);
        BtnDeleteProfile = view.findViewById(R.id.deleteBtnProfile);
        imageProfile = view.findViewById(R.id.ProfileSetting);
        new Thread(new Runnable() {
            @Override
            public void run() {
                imageProfile.setImageResource(R.drawable.itempictest);
            }
        }).start();

        BtnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                handleUpdateProfile();
            }
        });
        BtnAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavAboutUs();
            }
        });
        BtnDeleteProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleDeleteProfile();

            }
        });

        return view;
    }
    public void handleUpdateProfile(){
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new UserProfileDetailFragment())
                .addToBackStack(null)
                .commit();


    }

    public void NavAboutUs(){
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new AboutUsFragment())
                .addToBackStack(null)
                .commit();

    }
    public void handleDeleteProfile(){
        //

    }

}