package com.penhey.finalproject.Screen.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.ApiService.RetrofitClient;
import com.penhey.finalproject.Screen.ApiService.RetrofitInterface;
import com.penhey.finalproject.Screen.cart.SharedViewModel;
import com.penhey.finalproject.Screen.helper.GridSpacingItemDecoration;
import com.penhey.finalproject.Screen.Menu.MenuAdapter;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import com.penhey.finalproject.Screen.Menu.MenuItemsResponse;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment{
    private RecyclerView recyclerView;
    private MenuAdapter menuAdapter;
    private ProgressBar progressBar;
    private MenuAdapter.OnMenuItemAddCartListener itemAddCartListener;
    private SharedViewModel sharedViewModel;
    private ImageView imageprofile;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = view.findViewById(R.id.menuRecyclerView);
        progressBar = view.findViewById(R.id.progressBar);
        imageprofile = view.findViewById(R.id.imageProfile);
        new Thread(new Runnable() {
            @Override
            public void run() {
                imageprofile.setImageResource(R.drawable.itempictest);
            }
        }).start();

        itemAddCartListener = new MenuAdapter.OnMenuItemAddCartListener() {
            @Override
            public void onItemAddCart(MenuItemData menuItem) {
                sharedViewModel.addItemToCart(menuItem);
                Toast.makeText(getContext(), "Added to cart", Toast.LENGTH_SHORT).show();
            }
        };
        menuAdapter = new MenuAdapter(getContext(), new ArrayList<>(), this::navigateToDetail, itemAddCartListener);
        setupRecyclerView(view);
        loadMenuItems();
        return view;
    }


    private void setupRecyclerView(View view) {
        int spanCount = 2; // Number of columns in the grid
        int spacing = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
        GridLayoutManager layoutManager = new GridLayoutManager(requireContext(), spanCount);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, true));
        menuAdapter = new MenuAdapter(getContext(),new ArrayList<MenuItemData>() {
        }, this::navigateToDetail, itemAddCartListener);
        recyclerView.setAdapter(menuAdapter);

    }

    private void navigateToDetail(int menuID) {
        MenuDetailFragment detailFragment = MenuDetailFragment.newInstance(menuID);
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, detailFragment)
                .addToBackStack(null)
                .commit();
    }
    private void showLoadingIndicator(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showError(String message) {
        new AlertDialog.Builder(getContext())
                .setIcon(Integer.parseInt("@mipmap/error"))
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton("Retry", (dialog, which) -> loadMenuItems())
                .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                .show();
    }
    private void loadMenuItems() {

        showLoadingIndicator(true);
        RetrofitInterface retrofitInterface = RetrofitClient.getClient().create(RetrofitInterface.class);
        retrofitInterface.getAllMenuItems().enqueue(new Callback<MenuItemsResponse>() {
            @Override
            public void onResponse(@NonNull Call<MenuItemsResponse> call, @NonNull Response<MenuItemsResponse> response) {
                showLoadingIndicator(false);
                if (response.isSuccessful() && response.body() != null && response.body().isSuccess()) {
                    List<MenuItemData> items = response.body().getMenuItems();

                    menuAdapter.updateData(items); //  updateData accepts List<MenuItemData>
                } else {
                    showError("Failed to load menu items. Please try again.");
                }
            }
            @Override
            public void onFailure(@NonNull Call<MenuItemsResponse> call, @NonNull Throwable t) {
                showLoadingIndicator(false);
                showError("Error loading menu items: " + t.getMessage());
            }
        });
    }
    public void onResume() {
        super.onResume();
        // Set the input mode to adjustNothing or adjustPan when the fragment is visible
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // Reset to a neutral state or default behavior that makes sense for your app
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED);
        }
    }


}
