package com.penhey.finalproject.Screen.SignUp;

public class SignUpData {
    private String username;
    private String password;
    private String email;
    private String confirmPassword;
    private String role;
    private Integer userId;

    public SignUpData(String username, String email,String password, String confirmPassword, String role){
        this.username =username;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.role = role;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getUserRole(){
        return role;
    }

    public void setUserRole(){
        this.role = role;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}



