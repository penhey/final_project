package com.penhey.finalproject.Screen.cart;



import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.penhey.finalproject.R;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {
    private  TextView txtItemName, txtPrice, txtQuantity;
    private  ImageView imgItem;
    private Button btnIncrease, btnDecrease, btnDelete;
    private List<MenuItemData> cartItems;
    private final LayoutInflater inflater;
    private final OnCartItemInteractionListener interactionListener;

    public CartAdapter(Context context, List<MenuItemData> cartItems, OnCartItemInteractionListener interactionLister) {
        this.inflater = LayoutInflater.from(context);
        this.cartItems = cartItems == null ? new ArrayList<>() : cartItems;
        this.interactionListener = interactionLister;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.add_cart_item, parent, false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        MenuItemData menuItem = cartItems.get(position);
        holder.bind(menuItem);
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public void updateData(List<MenuItemData> newCartItems) {
        // Capture the old list
        final List<MenuItemData> oldCartItems = new ArrayList<>(this.cartItems);
        this.cartItems = newCartItems;

        // Calculate the diff and dispatch updates to the adapter
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new CartDiffCallback(oldCartItems, newCartItems));
        diffResult.dispatchUpdatesTo(this);
    }


    public class CartViewHolder extends RecyclerView.ViewHolder {
        TextView txtItemName, txtPrice, txtQuantity;
        ImageView imgItem;
        Button btnIncrease, btnDecrease, btnDelete;
        SharedViewModel sharedViewModel;

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemName = itemView.findViewById(R.id.item_name);
            txtPrice = itemView.findViewById(R.id.item_price);
            txtQuantity = itemView.findViewById(R.id.quantity_text_view);
            imgItem = itemView.findViewById(R.id.item_image_cart);
            btnIncrease = itemView.findViewById(R.id.increase_button);
            btnDecrease = itemView.findViewById(R.id.decrease_button);
            btnDelete = itemView.findViewById(R.id.delete_button);

        }

        void bind(final MenuItemData menuItem) {
            txtItemName.setText(menuItem.getItemName());
            txtPrice.setText(String.format("$%s",menuItem.getPrice()));
            txtQuantity.setText(String.valueOf(menuItem.getQuantity()));
            Picasso.get().load(menuItem.getImageURL()).into(imgItem);
            btnDelete.setTextColor(Color.RED);


            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) { // Ensure the position is valid
                        cartItems.remove(position); // Remove the item from the data set
                        notifyItemRemoved(position); // Notify the adapter of item removal
                        notifyItemRangeChanged(position, cartItems.size() - position);
                        // Update the range for any subsequent items
                        // Optionally, if you have an interaction listener to handle further actions (like updating a ViewModel), invoke it here
                        if(interactionListener != null) {
                            interactionListener.onItemRemoved(menuItem);
                        }

                    }


                }
            });

            //btnDecrease.setBackgroundColor(Color.LTGRAY);

            btnDecrease.setOnClickListener(v -> {
                if (menuItem.getQuantity() > 1) {
                    menuItem.decreaseQuantity();
                    txtQuantity.setText(String.valueOf(menuItem.getQuantity()));
                    interactionListener.onItemQuantityChanged(menuItem);
                    notifyItemChanged(getAdapterPosition()); // Notify to reflect the change
                }
            });
            //btnIncrease.setBackgroundColor(Color.LTGRAY);
            btnIncrease.setOnClickListener(v -> {
                if(menuItem.getQuantity()>0){
                    menuItem.increaseQuantity();
                    txtQuantity.setText(String.valueOf(menuItem.getQuantity()));
                    interactionListener.onItemQuantityChanged(menuItem);
                    notifyItemChanged(getAdapterPosition()); // Notify to reflect the change
                }
            });
        }
    }

    public interface OnCartItemInteractionListener {
        void onItemQuantityChanged(MenuItemData menuItem);
        void onItemRemoved(MenuItemData menuItem);

    }
}



