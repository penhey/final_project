package com.penhey.finalproject.Screen.Menu;// MenuAdapter.java


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.penhey.finalproject.R;
import java.util.List;

public class MenuAdapterMock extends RecyclerView.Adapter<MenuAdapterMock.ViewHolder> {

    private Context context;
    private List<MenuItemMock> menuItems;

    public MenuAdapterMock(Context context, List<MenuItemMock> menuItems) {
        this.context = context;
        this.menuItems = menuItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MenuItemMock menuItem = menuItems.get(position);

        holder.imageView.setImageResource(menuItem.getImageResId());
        holder.nameView.setText(menuItem.getName());
        //holder.descriptionView.setText(menuItem.getDescription());
        holder.priceView.setText(menuItem.getPrice());

        holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle adding item to the cart
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameView;
        TextView descriptionView;
        TextView priceView;
        ImageButton btnAddToCart;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageMenuView);
            nameView = itemView.findViewById(R.id.nameView);
            //descriptionView = itemView.findViewById(R.id.descriptionView);
            priceView = itemView.findViewById(R.id.priceView);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
        }


    }

    /*public static class ViewMenuDetail extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView nameView;
        TextView descriptionView;
        TextView priceView;
        ImageButton btnAddToCart;


        public ViewMenuDetail(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageMenuView);
            nameView = itemView.findViewById(R.id.nameView);
            //descriptionView = itemView.findViewById(R.id.descriptionView);
            priceView = itemView.findViewById(R.id.priceView);
            btnAddToCart = itemView.findViewById(R.id.btnAddToCart);
        }

    }

     */
}
