package com.penhey.finalproject.Screen.helper;

import androidx.recyclerview.widget.DiffUtil;

import com.penhey.finalproject.Screen.Menu.MenuItemData;

import java.util.List;

public class MenuDiffCallback extends DiffUtil.Callback {

    private final List<MenuItemData> oldList;
    private final List<MenuItemData> newList;

    public MenuDiffCallback(List<MenuItemData> oldList, List<MenuItemData> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        // Implement your logic to check if items are the same
        return oldList.get(oldItemPosition).getMenuID() == newList.get(newItemPosition).getMenuID();
    }
    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        // Here, you can improve by checking individual fields to determine if the contents are the same.
        MenuItemData oldItem = oldList.get(oldItemPosition);
        MenuItemData newItem = newList.get(newItemPosition);

        return oldItem.getMenuID() == newItem.getMenuID() &&
                oldItem.getItemName().equals(newItem.getItemName()) &&
                oldItem.getDescription().equals(newItem.getDescription()) &&
                oldItem.getPrice().equals(newItem.getPrice()) &&
                oldItem.getImageURL().equals(newItem.getImageURL());
    }
}

