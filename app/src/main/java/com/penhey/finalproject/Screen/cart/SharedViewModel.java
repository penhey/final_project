package com.penhey.finalproject.Screen.cart;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.penhey.finalproject.Screen.Menu.MenuItemData;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<List<MenuItemData>> cartItems = new MutableLiveData<>(new ArrayList<>());

    // Method to add item to the cart
    public void addItemToCart(MenuItemData menuItem) {
        List<MenuItemData> currentCartItems = cartItems.getValue();
        if (currentCartItems == null) currentCartItems = new ArrayList<>();
        int index = -1;
        for (int i = 0; i < currentCartItems.size(); i++) {
            if (Objects.equals(currentCartItems.get(i).getMenuID(), menuItem.getMenuID())) { // Assuming MenuItemData has getId()
                index = i;
                break;
            }
        }
        if (index != -1) {
            // If it exists, increase the quantity
            MenuItemData existingItem = currentCartItems.get(index);
            existingItem.setQuantity(existingItem.getQuantity() + 1);
        } else {
            // If it doesn't exist, add the item with quantity 1
            menuItem.setQuantity(1);
            currentCartItems.add(menuItem);
        }
        cartItems.setValue(currentCartItems); // Trigger the observer
    }

    public LiveData<List<MenuItemData>> getCartItems() {
        return cartItems;
    }

    public void adjustItemQuantity(MenuItemData menuItem, int quantity) {
        List<MenuItemData> currentCartItems = cartItems.getValue();
        if (currentCartItems == null) return;
        for (MenuItemData item : currentCartItems) {
            if (Objects.equals(item.getMenuID(), menuItem.getMenuID())) {
                item.setQuantity(quantity);
                break;
            }
        }
        cartItems.setValue(currentCartItems); // Trigger the observer
    }

    public void removeItemFromCart(MenuItemData menuItem) {
        List<MenuItemData> currentCartItems = cartItems.getValue();
        if (currentCartItems != null) {
            List<MenuItemData> updatedCartItems = new ArrayList<>(currentCartItems);
            currentCartItems.removeIf(item-> Objects.equals(item.getMenuID(), menuItem.getMenuID()));
            cartItems.setValue(updatedCartItems); // Ensure LiveData is updated
        }

    }


}
