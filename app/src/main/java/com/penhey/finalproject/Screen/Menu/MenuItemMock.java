package com.penhey.finalproject.Screen.Menu;

// MenuItem.java
public class MenuItemMock {

    private int imageResId;
    private String name;
    private String description;
    private String price;

    public MenuItemMock(int imageResId, String name, String price) {
        this.imageResId = imageResId;
        this.name = name;
       // this.description = description;
        this.price = price;
    }
    public MenuItemMock(int imageResId, String name, String description, String price ){
        this.imageResId = imageResId;
        this.name = name;
        this.description = description;
        this.price = price;

    }


    public int getImageResId() {
        return imageResId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }
}

