package com.penhey.finalproject.Screen.Menu;

import com.google.gson.annotations.SerializedName;

public class MenuItemData {

    private int quantity; // Quantity field


    @SerializedName("MenuID")
    private int menuID;

    @SerializedName("ItemName")
    private String itemName;

    @SerializedName("Description")
    private String description;

    @SerializedName("Price")
    private String price;

    @SerializedName("ImageURL")
    private String imageURL;
    public MenuItemData() {
        this.menuID = menuID;
        this.itemName = itemName;
        this.description = description;
        this.price = price;
        this.imageURL = imageURL;
    }

    public MenuItemData(int menuId, String itemName, String description, String price, String imageURL) {
        this.menuID = menuId;
        this.itemName = itemName;
        this.description = description;
        this.price = price;
        this.imageURL = imageURL;
    }

    public void setMenuID(int menuID) {
        this.menuID = menuID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
    public int getMenuID() {
        return menuID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void decreaseQuantity(){
        if(this.quantity > 1){
            this.quantity--;
        }
    }

    public void increaseQuantity() {
        this.quantity++; // Allows increase from any positive number, including 0
    }




}

